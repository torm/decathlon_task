#!/bin/bash

cd /home/decathlon_task

./wait-for-it.sh decathlon_task_db:5432

python manage.py makemigrations --settings=decathlon_task.settings_production
python manage.py migrate --settings=decathlon_task.settings_production
python manage.py collectstatic --settings=decathlon_task.settings_production

uwsgi --http :8000 --module decathlon_task.wsgi --static-map /static=/home/decathlon_task/static --mime-file mime.types
