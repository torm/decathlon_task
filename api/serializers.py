from rest_framework import serializers

from system.models import Movie, Comment


class MovieSerializer(serializers.ModelSerializer):
    def validate(self, data):
        if 'Response' in data['data']:
            response = data['data'].pop('Response')
            if response == 'False':
                raise serializers.ValidationError(data['data']['Error'])
        return data

    def create(self, validated_data):
        # sort working better on numeric data, in real life all values should be cast
        if 'data' in self.validated_data and 'imdbRating' in self.validated_data['data']:
            self.validated_data['data']['imdbRating'] = float(self.validated_data['data']['imdbRating'])

        movie, _ = Movie.objects.get_or_create(**validated_data)
        return movie

    class Meta:
        model = Movie
        fields = ('id', 'data')


class SearchMovieSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=200)


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('id', 'movie', 'content')
