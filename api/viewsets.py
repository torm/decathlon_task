import requests

from django.conf import settings
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend

from system.models import Movie, Comment
from api.serializers import MovieSerializer, SearchMovieSerializer, CommentSerializer
from api.filters import MovieFilter


class MovieViewSet(viewsets.ModelViewSet):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    filterset_class = MovieFilter
    ordering_fields = ('data__imdbRating',)

    def create(self, request, *args, **kwargs):
        deserializer = SearchMovieSerializer(data=request.data)
        deserializer.is_valid(raise_exception=True)

        url = '%s?apikey=%s&t=%s' % (settings.OMDB_URL, settings.OMDB_APIKEY, deserializer.validated_data['title'])
        response = requests.get(url)

        serializer = MovieSerializer(data={'data': response.json()})
        serializer.is_valid(raise_exception=True)
        serializer.save()

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class CommentViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('movie',)
