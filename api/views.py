from rest_framework.views import APIView
from rest_framework.response import Response

from system.models import Movie


class Top(APIView):
    def get(self, request):
        return Response(Movie.objects.with_ranking().values('movie_id', 'total_comments', 'rank'))
