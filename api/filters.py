from django_filters import rest_framework as filters
from django_filters.filters import CharFilter

from system.models import Movie


class MovieFilter(filters.FilterSet):
    """Filter for Books by if books are published or not"""
    title = CharFilter(method='filter_icontains', label='Title')
    type = CharFilter(method='filter_exactly', label='Type')

    def filter_icontains(self, queryset, name, value):
        return queryset.filter(**{'data__%s__icontains' % name.title(): value})

    def filter_exactly(self, queryset, name, value):
        return queryset.filter(**{'data__%s' % name.title(): value})

    class Meta:
        model = Movie
        exclude = ['data']
