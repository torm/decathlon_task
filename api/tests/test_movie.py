from django.urls import reverse
from rest_framework.test import APITestCase

from system.models import Movie


class TestMovie(APITestCase):

    def setUp(self):
        Movie.objects.get_or_create(data={'Title': 'Movie01', 'imdbRating': 1.4, 'Type': 'movie'})
        Movie.objects.get_or_create(data={'Title': 'Movie02', 'imdbRating': 0.2, 'Type': 'serial'})
        Movie.objects.get_or_create(data={'Title': 'Movie03', 'imdbRating': 3, 'Type': 'serial'})

        self.movie_count = Movie.objects.count()

    def test_add_movie_correct_title(self):
        response = self.client.post(reverse('movie-list'), data={'title': 'Matrix'})
        response_dict = response.json()

        self.assertEqual(response.status_code, 201)
        self.assertEqual(Movie.objects.count(), self.movie_count+1)
        self.assertEqual(response_dict['data']['Title'], 'Matrix')
        self.assertEqual(response_dict['data']['Runtime'], '60 min')

    def test_add_movie_incorrect_title(self):
        response = self.client.post(reverse('movie-list'), data={'title': 'adasdas'})

        self.assertEqual(response.status_code, 400)
        self.assertEqual(Movie.objects.count(), self.movie_count)

    def test_add_movie_empty_title(self):
        response = self.client.post(reverse('movie-list'), data={'title': ''})

        self.assertEqual(response.status_code, 400)
        self.assertEqual(Movie.objects.count(), self.movie_count)

    def test_add_movie_duplicate(self):
        self.client.post(reverse('movie-list'), data={'title': 'Matrix'})
        self.client.post(reverse('movie-list'), data={'title': 'Matrix'})

        self.assertEqual(Movie.objects.count(), self.movie_count+1)

    def test_add_movie_imdbRating_cast(self):
        self.client.post(reverse('movie-list'), data={'title': 'Matrix'})
        movie = Movie.objects.get(data__Title='Matrix')

        self.assertEqual(type(movie.data['imdbRating']), float)

    def test_delete_movie_correct_id(self):
        movie = Movie.objects.first()
        response = self.client.delete(reverse('movie-detail', args=[movie.pk]))

        self.assertEqual(response.status_code, 204)
        self.assertEqual(Movie.objects.count(), self.movie_count-1)

    def test_delete_movie_incorrect_id(self):
        response = self.client.delete(reverse('movie-detail', args=[12]))

        self.assertEqual(response.status_code, 404)
        self.assertEqual(Movie.objects.count(), self.movie_count)

    def test_update_movie(self):
        movie = Movie.objects.first()
        response = self.client.put(
            reverse('movie-detail', args=[movie.id]), data={'data': {'Title': 'MMM'}}, format='json')
        response_dict = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response_dict['data']['Title'], 'MMM')
        self.assertEqual(Movie.objects.count(), self.movie_count)

    def test_sort_movies_imdbRating(self):
        response = self.client.get(reverse('movie-list') + '?ordering=-data__imdbRating')
        response_dict = response.json()

        self.assertEqual(response_dict[0]['data']['Title'], 'Movie03')
        self.assertEqual(response_dict[1]['data']['Title'], 'Movie01')
        self.assertEqual(response_dict[2]['data']['Title'], 'Movie02')

    def test_filter_movies_type_correct(self):
        response = self.client.get(reverse('movie-list') + '?type=movie')
        response_dict = response.json()

        self.assertEqual(response_dict[0]['data']['Title'], 'Movie01')
        self.assertEqual(len(response_dict), 1)

    def test_filter_movies_type_incorrect(self):
        response = self.client.get(reverse('movie-list') + '?type=movies')
        response_dict = response.json()

        self.assertEqual(len(response_dict), 0)

    def test_filter_movies_title(self):
        response = self.client.get(reverse('movie-list') + '?title=movie01')
        response_dict = response.json()

        self.assertEqual(response_dict[0]['data']['Title'], 'Movie01')
        self.assertEqual(len(response_dict), 1)

        response = self.client.get(reverse('movie-list') + '?title=movie')
        response_dict = response.json()

        self.assertEqual(len(response_dict), self.movie_count)
