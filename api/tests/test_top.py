from django.urls import reverse
from rest_framework.test import APITestCase

from system.models import Movie


class TestTop(APITestCase):

    def setUp(self):
        Movie.objects.get_or_create(data={'Title': 'Movie01', 'imdbRating': 1.4, 'Type': 'movie'})
        Movie.objects.get_or_create(data={'Title': 'Movie02', 'imdbRating': 0.2, 'Type': 'serial'})
        Movie.objects.get_or_create(data={'Title': 'Movie03', 'imdbRating': 3, 'Type': 'serial'})

        self.movie_count = Movie.objects.count()

    def test_add_movie_correct_title(self):
        response = self.client.get(reverse('top'))
        response_dict = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response_dict), self.movie_count)
        self.assertIn('total_comments', response_dict[0].keys())
        self.assertIn('rank', response_dict[0].keys())
