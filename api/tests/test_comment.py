from django.urls import reverse
from rest_framework.test import APITestCase

from system.models import Movie, Comment


class TestComment(APITestCase):

    def setUp(self):
        m1, _ = Movie.objects.get_or_create(data={'Title': 'Movie01', 'imdbRating': "1.4", 'Type': 'movie'})
        m2, _ = Movie.objects.get_or_create(data={'Title': 'Movie02', 'imdbRating': 0.2, 'Type': 'serial'})
        Movie.objects.get_or_create(data={'Title': 'Movie03', 'imdbRating': 3, 'Type': 'serial'})

        Comment.objects.get_or_create(movie=m1, content='Comment01')
        Comment.objects.get_or_create(movie=m1, content='Comment02')
        Comment.objects.get_or_create(movie=m1, content='Comment03')
        Comment.objects.get_or_create(movie=m1, content='Comment04')
        Comment.objects.get_or_create(movie=m2, content='Comment05')
        Comment.objects.get_or_create(movie=m2, content='Comment06')

        self.movie_count = Movie.objects.count()
        self.comment_count = Comment.objects.count()

    def test_add_comment(self):
        m = Movie.objects.first()

        response = self.client.post(reverse('comment-list'), data={'movie': m.id, 'content': 'Comment99'})
        response_dict = response.json()

        self.assertEqual(response.status_code, 201)
        self.assertEqual(Comment.objects.count(), self.comment_count+1)
        self.assertEqual(Movie.objects.count(), self.movie_count)
        self.assertEqual(response_dict['movie'], m.id)
        self.assertEqual(response_dict['content'], 'Comment99')

    def test_add_comment_empty_content(self):
        m = Movie.objects.first()

        response = self.client.post(reverse('comment-list'), data={'movie': m.id, 'content': ''})

        self.assertEqual(response.status_code, 400)
        self.assertEqual(Comment.objects.count(), self.comment_count)

    def test_add_comment_incorrect_move_id(self):
        response = self.client.post(reverse('comment-list'), data={'movie': 77, 'content': ''})

        self.assertEqual(response.status_code, 400)
        self.assertEqual(Comment.objects.count(), self.comment_count)

    def test_comment_list(self):
        response = self.client.get(reverse('comment-list'))
        response_dict = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response_dict), self.comment_count)

    def test_filter_comment_by_movie_id_correct(self):
        m = Movie.objects.get(data__Title='Movie01')

        response = self.client.get(reverse('comment-list') + '?movie=%d' % m.id)
        response_dict = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response_dict[0]['movie'], m.id)
        self.assertEqual(len(response_dict), m.comments.count())

    def test_filter_comment_by_movie_id_incorrect(self):
        response = self.client.get(reverse('comment-list') + '?movie=%d' % 100)
        self.assertEqual(response.status_code, 400)
