FROM python:3.7-slim-stretch

RUN apt-get update
RUN apt-get install -y git libpq-dev gcc

#WORKDIR /home
#RUN git clone https://torm@bitbucket.org/torm/decathlon_task.git

RUN mkdir decathlon_task
ADD . /home/decathlon_task

WORKDIR /home/decathlon_task
RUN chmod +x startup.sh
RUN chmod +x wait-for-it.sh
RUN pip install -r requirements.txt
RUN pip install uwsgi