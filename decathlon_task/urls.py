from django.urls import include, path
from rest_framework import routers

from api.viewsets import MovieViewSet, CommentViewSet
from api.views import Top

router = routers.DefaultRouter()
router.register(r'movies', MovieViewSet)
router.register(r'comments', CommentViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('top/', Top.as_view(), name='top'),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
