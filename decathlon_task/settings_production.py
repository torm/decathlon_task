from decathlon_task.settings import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['decathlontask.torm01.eu', 'localhost']

DATABASES = {
    'default': {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": "decathlon_task",
        "USER": "postgres",
        "PASSWORD": "postgres",
        "HOST": "decathlon_task_db",
        "PORT": "5432",
    }
}

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, "static")
