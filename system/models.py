from django.db import models
from django.db.models import Count, F
from django.db.models.functions import DenseRank
from django.db.models.expressions import Window
from django.contrib.postgres.fields import JSONField


class MovieQuerySet(models.QuerySet):
    def with_ranking(self):
        return self.annotate(
            movie_id=F('id'), total_comments=Count('comments'), rank=Window(
                expression=DenseRank(), order_by=F('total_comments').desc()
            )
        )


class Movie(models.Model):
    data = JSONField()

    objects = MovieQuerySet.as_manager()


class Comment(models.Model):
    movie = models.ForeignKey(Movie, related_name='comments', on_delete=models.CASCADE)
    content = models.CharField(max_length=500)
