import requests

from django.test import TestCase
from django.conf import settings


class TestOmdb(TestCase):

    def test_Omdb_api_response(self):
        url = '%s?apikey=%s&i=tt3896198' % (settings.OMDB_URL, settings.OMDB_APIKEY)

        response = requests.get(url)
        response_dict = response.json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response_dict.get('Title'), 'Guardians of the Galaxy Vol. 2')
        self.assertEqual(response_dict.get('Runtime'), '136 min')
        self.assertEqual(response_dict.get('Director'), 'James Gunn')
