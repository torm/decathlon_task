from django.test import TestCase

from system.models import Movie, Comment


class TestMovie(TestCase):

    def setUp(self):
        # in real project a factory from `factory_boy` would be better
        m1, _ = Movie.objects.get_or_create(data={'Title': 'Movie01', 'imdbRating': "1.4", 'Type': 'movie'})
        m2, _ = Movie.objects.get_or_create(data={'Title': 'Movie02', 'imdbRating': 0.2, 'Type': 'serial'})
        m3, _ = Movie.objects.get_or_create(data={'Title': 'Movie03', 'imdbRating': 3, 'Type': 'serial'})

        self.movies = [m1, m2, m3]
        self.movie_count = Movie.objects.count()

    def test_movie_with_ranking_total_comments(self):
        for idx, m in enumerate(self.movies):
            for i in range(idx):
                Comment.objects.get_or_create(movie=m, content='bla '*i)

        movies = Movie.objects.with_ranking()

        for idx, m in enumerate(self.movies):
            self.assertEqual(movies.get(id=m.id).total_comments, idx)

    def test_movie_with_ranking_zero_comments(self):
        movies = Movie.objects.with_ranking()
        ranks = [m.rank for m in movies]

        self.assertEqual(max(ranks), min(ranks))
        self.assertEqual(max(ranks), 1)
        self.assertEqual(sum(m.total_comments for m in movies), 0)

    def test_movie_with_ranking_same_number_of_comments(self):
        for m in self.movies:
            Comment.objects.get_or_create(movie=m, content='bla bla bla')
        movies = Movie.objects.with_ranking()
        ranks = [m.rank for m in movies]

        self.assertEqual(max(ranks), min(ranks))
        self.assertEqual(max(ranks), 1)
        self.assertEqual(sum(m.total_comments for m in movies), len(self.movies))

    def test_movie_with_ranking_gap(self):
        for m in self.movies[:2]:
            Comment.objects.get_or_create(movie=m, content='bla bla bla')
            Comment.objects.get_or_create(movie=m, content='bla bla bla bla')

        movies = Movie.objects.with_ranking()
        ranks = [m.rank for m in movies]

        self.assertEqual(sorted(ranks)[:3], [1, 1, 2])
        self.assertEqual(sum(m.total_comments for m in movies), 2*2)

