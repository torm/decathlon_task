## Movie application - decathlon requirement task

##### local deployment
Requirements: `docker`, `docker-compose`

1 Build docker image or pull from dockerhub
```bash
cd decathlon_task
docker build --no-cache -t tomaszrycz/decathlon_task .

or

docker pull tomaszrycz/decathlon_task

```
2 Run stack
```bash
docker-compose -f stack.yml up
```
After that application should be available on `http://localhost:8000`. 
**WARNING: If container `decathlon_task_db` is removed all data will be lost.**

##### endpoints overview
* POST `movie/` data: `{"title": <movie_title>}`
* GET `movie/?title=&type=&ordering=<data__imdbRating>`
* DELETE `movie/<id>` 
* PUT `movie/<id>` 
* POST`comments/` data: `{"movie": <movie_id>, "content": <comment content>}`
* GET`comments/?movie=` (movie is a id of movie)
* GET `top/`

##### 3rd part library
* django
* psycopg2 - driver for database connetion
* djangorestframework - rest api framework for django
* requests - HTTP library for Python
* django-filter - library for advance filtring
